# k8s-hello-node

Container image and source for Hello minikube tutorial https://kubernetes.io/docs/tutorials/hello-minikube/

# Single replica

## Deployment

* Imperative command :  `kubectl create deployment hello-node --image=registry.gitlab.com/bit-man/k8s-hello-node`
* Imperative object configuration : `kubectl apply -f https://gitlab.com/bit-man/k8s-hello-node/raw/master/single-replica/deploy.yml`
* Declarative object configuration : `kubectl apply -f ${k8s-hello-node-git-repo-clone-folder}/single-replica/`

For declarative deployment use [API documentation](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#deployment-v1-apps)

## Testing

    # export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
    # kubectl exec -it $POD_NAME  curl localhost:8080
    Hello World!


# Multiple replica

## Deployment

* Imperative commands

      # kubectl create deployment hello-node --image=registry.gitlab.com/bit-man/k8s-hello-node
      # kubectl expose deployment/hello-node --type="NodePort" --port 8080

  First command creates deplyment and second one creates service exposing pods application at port 8080

* Imperative object configuration

      # kubectl apply -f https://gitlab.com/bit-man/k8s-hello-node/raw/master/multiple-replica/deploy.yml -f https://gitlab.com/bit-man/k8s-hello-node/raw/master/multiple-replica/service.yml

* Declarative object configuration : `kubectl apply -f ${k8s-hello-node-git-repo-clone-folder}/multiple-replica/`

For declarative service use [API documentation](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#service-v1-core)

## Testing

    # export NODE_PORT=$(kubectl get services/hello-node -o go-template='{{(index .spec.ports 0).nodePort}}')
    # curl $(minikube ip):$NODE_PORT
    Hello World!
